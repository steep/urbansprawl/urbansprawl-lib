#
# docker build -f urbansprawl.dockerfile -t pissard/urbansprawl .
# docker run --name urbansprawl -it --rm -v $HOME/Dev/Steep/urbansprawl-lib/:/urbansprawl -p 8888:8888 pissard/urbansprawl
#
# jupyter lab --ip=0.0.0.0 
#

FROM ubuntu:latest
RUN apt-get update
RUN apt-get install -y build-essential
RUN apt-get install -y libspatialindex-dev python3 python3-pip
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1 \
    && update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1
RUN pip install osmnx scikit-learn psutil tensorflow keras jupyterlab geopy
RUN pip install networkx==2.3
RUN pip install geopandas==0.4.1

VOLUME /urbansprawl
RUN addgroup --gid 50101 sed\
    && useradd pissard --uid 50107 --gid 50101 -d /home/pissard \
    && mkdir /home/pissard \
    && chown pissard:sed /home/pissard
WORKDIR /urbansprawl
USER pissard

EXPOSE 8888
