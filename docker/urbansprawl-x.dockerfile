#
# docker build -f urbansprawl-x.dockerfile -t pissard/urbansprawl-x .
# docker run --name urbansprawl-x -ti -v $HOME/Dev/Steep/urbansprawl/:/urbansprawl -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY pissard/urbansprawl-x
#
# su developper
# jupyter notebook --ip=0.0.0.0 


FROM ubuntu:bionic
RUN apt-get update
RUN apt-get install -y build-essential
RUN apt-get install -y libspatialindex-dev python3 python3-pip
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1 \
    && update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1
RUN pip install osmnx scikit-learn psutil tensorflow keras jupyter
RUN pip install networkx==2.3
RUN apt-get install -y firefox libcanberra-gtk*

VOLUME /urbansprawl
RUN useradd developper --uid 1000 -d /home/developper \
    && mkdir /home/developper \
    && chown developper:developper /home/developper
WORKDIR /urbansprawl
